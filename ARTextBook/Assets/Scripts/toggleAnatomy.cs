﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class toggleAnatomy : MonoBehaviour
{
    public GameObject AnatomyObject;
    public GameObject SkeletonObject;

    public void ToggleModel()
    {
        if(AnatomyObject.activeSelf == true)
        {
            SkeletonObject.SetActive(true);
            AnatomyObject.SetActive(false);
        }
        else
        {
            SkeletonObject.SetActive(false);
            AnatomyObject.SetActive(true);
        }
    }
}
